<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Ma page avec php</title>
    </head>
    <body>
<!--Exo 1 : Comment tu t'appelles ?
Demander le prénom
vérifier que le prénom fait entre 1 et 10 caractères
afficher un message du type "Bonjour [prénom] !"-->
        <h1>Ma page web</h1>

        <p>Tout d'abord, quel est ton nom:</p> 
  

<form>

    <input type="text" name="prenom" minlength="1" maxlength="10" placeholder="entre 1 et 10 caractères"/>
    <input type="submit" value="Valider" />

</form>
    <p> Bonjour <?php echo $_GET['prenom']; ?>, comment vas-tu ?.</p>
    <?php
        $name = $_GET['prenom'];

        if (strlen($name) >=7)
    {
        echo "Tu as un sacré nom";
    } else if (strlen($name) <=6)
    {
        echo "Tu as un joli prénom";
    } else {
        return;
    }
    ?>

<br><br>
        
<!--Exo 2 : Login/mot de passe
Créer un formulaire d'authentification par identifiant et mot de passe
l'identifiant doit contenir plus de 4 caractères, et un arobase
l'identifiant doit être "lea@gmail.com"
le mot de passe doit être "abE353_5"
un message doit dire si l'authentification a réussit ou non-->

<input type="text" name="login" minlength="1" maxlength="25" placeholder="Nom d'utilisateur"/>
<input type="password" name="password" placeholder="Mot de passe"/>
<input type="submit" value="Valider" />

<?php
    if ($login == "lea@gmail.com" && $password == "abE353_5") {
        echo "Ravie de vous revoir !";
    };
    if ($login != "lea@gmail.com" || $password != "abE353_5") {
        echo "Identifiants incorrects";
    };
    if(!empty($login) || !empty($password))
    {
        echo "Veuillez entrer vos identifiants.";
    };

?>


<!--Exo 4 : calcul
le programme tire aléatoirement une opération (+, -, x) et deux chiffres
le programme demande à l'utilisateur la réponse
puis dit si le résultat est bon ou faux !-->



<!--Exo 5 : moyenne
demander à l'utilisateur de saisir 10 notes
afficher en retour la moyenne-->

<!--Exo 6 : nombre mystère
le programme tire un nombre aléatoirement entre 0 et 10
l'utilisateur donne un nombre
le programme répond "plus", "moins", ou "gagné !"
le programme tourne en boucle jusqu'à ce que l'utilisateur ait trouvé la bonne valeur-->

    </body>
</html>